<form action="contact/mailer.php" method="post" name="form1" id="form1" onsubmit="MM_validateForm('from','','RisEmail','subject','','R','verif_box','','R','message','','R');return document.MM_returnValue">
	
	<h3>I would like <strong>more information</strong></h3>

<input name="name" type="text" placeholder="YOUR NAME" id="name" class="contact-main-input" value="<?php echo $_GET['name'];?>"/>
<br />

<input name="from" type="text" placeholder="EMAIL ADDRESS" id="from" class="contact-main-input" value="<?php echo $_GET['from'];?>"/>
<br />

<!--<input name="subject" type="text" placeholder="SUBJECT" id="subject" class="contact-main-input" value="<?php echo $_GET['subject'];?>"/>
<br />-->

<textarea name="message" cols="6" placeholder="MESSAGE" rows="5" id="message" class="contact-message-input"><?php echo $_GET['message'];?></textarea>
	
<input name="verif_box" type="text" placeholder="ENTER CODE" id="verif_box" class="contact-verif-code"/>
<div class="contact-verif-image"><img src="contact/verificationimage.php?<?php echo rand(0,9999);?>" alt="verification image, type it in the box" width="70" height="24" align="top" /></div>

<?php if(isset($_GET['wrong_code'])){?>
<div class="prompt">Try those numbers again &uarr;</div>
<?php ;}?>
	
<div class="contact-button-area">
<button name="Submit" type="submit" class="contact-send-button" value="SEND">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="52px" height="52px" viewBox="0 0 268.832 268.832" style="enable-background:new 0 0 268.832 268.832;" xml:space="preserve" class=""><g><g>
	<path d="M265.171,125.577l-80-80c-4.881-4.881-12.797-4.881-17.678,0c-4.882,4.882-4.882,12.796,0,17.678l58.661,58.661H12.5   c-6.903,0-12.5,5.597-12.5,12.5c0,6.902,5.597,12.5,12.5,12.5h213.654l-58.659,58.661c-4.882,4.882-4.882,12.796,0,17.678   c2.44,2.439,5.64,3.661,8.839,3.661s6.398-1.222,8.839-3.661l79.998-80C270.053,138.373,270.053,130.459,265.171,125.577z" data-original="#242424" class="active-path" fill="#242424"/>
</g></g> </svg>	
</button>
</div>
	
<?php if(isset($_GET['message_sent'])){?>
<div class="thanks"><strong>Thanks!</strong> We'll be in touch shortly.</div>
<?php ;}?>
	
</form>
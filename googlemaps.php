<!-- Digital Design Works — Custom map design-->
<div class="byeGoogle"></div>
<div id="map" class=""></div>
<style>
	.gmnoprint, .gm-style-cc {
		display: none;
	}
	.byeGoogle {
		width: 80px;
		height: 35px;
		background: #010101;
		position: absolute;
		bottom: 0;
		left: 50%;
		z-index: 1;
		border-top-right-radius: 30px;
	}
	@media screen and (max-width: 990px) {
		.byeGoogle {
			left: 0;
		}
	}
</style>
<script>
	function initMap() {
		
		var myLatLng = {lat: 51.1947327, lng: -0.0667021};
		
		var map = new google.maps.Map(document.getElementById('map'), {
			center: myLatLng,
			zoom: 6,
			scrollwheel: false,
			zoomControl: true,
			disableDoubleClickZoom: true,
			fullscreenControl: false,
			styles: [
				{elementType: 'geometry', stylers: [{color: '#151515'}]},
				{elementType: 'labels.text.stroke', stylers: [{visibility: 'off'}]},
				{elementType: 'labels.text.fill', stylers: [{color: '#333333'}]},
				{
					featureType: 'administrative.locality',
					elementType: 'labels.text.fill',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'poi',
					elementType: 'labels.text.fill',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'poi.park',
					elementType: 'geometry',
					stylers: [{color: '#151515'}]
				},
				{
					featureType: 'poi.park',
					elementType: 'labels.text.fill',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'road',
					elementType: 'labels',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'road',
					elementType: 'geometry',
					stylers: [{color: '#010101'}]
				},
				{
					featureType: 'road',
					elementType: 'geometry.stroke',
					stylers: [{color: '#010101'}]
				},
				{
					featureType: 'road',
					elementType: 'labels.text.fill',
					stylers: [{color: '#010101'}]
				},
				{
					featureType: 'road.highway',
					elementType: 'geometry',
					stylers: [{color: '#151515'}]
				},
				{
					featureType: 'road.highway',
					elementType: 'geometry.stroke',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'road.highway',
					elementType: 'labels.text.fill',
					stylers: [{color: '#010101'}]
				},
				{
					featureType: 'transit',
					elementType: 'geometry',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'transit.station',
					elementType: 'labels.text.fill',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'water',
					elementType: 'geometry',
					stylers: [{color: '#010101'}]
				},
				{
					featureType: 'water',
					elementType: 'labels.text.fill',
					stylers: [{visibility: 'off'}]
				},
				{
					featureType: 'water',
					elementType: 'labels.text.stroke',
					stylers: [{visibility: 'off'}]
				}
			]
		});
		
		var funkTown = {
			path: 'M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0',
			fillColor: '#EA6429',
			fillOpacity: 1,
			strokeColor: '',
			strokeWeight: 0,
			scale: 0.5
		};
		
		var customMarker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Mark Roberts Motion Control',
			icon: funkTown
		});

	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlSEhDAUrQ0pblOAyTBiOLqevMUwUX9qI&callback=initMap"
				async defer></script>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>The Bolt Junior | High Speed Cinebot</title>
		<meta name="description" content="Bolt Jr. is a compact, lighter weight high-speed camera robot that’s an agile, smaller option than the full scale MRMC Bolt. The Bolt Jr. is an ideal solution where space, weight, mobility and budget are key." />
		<meta name="keywords" content="bolt, Bolt Jr., cinebot, high-speed, robot, cinema, video rig" />
		<meta name="author" content="Digital Design Works" />
		<meta name="format-detection" content="telephone=no" />
		<link rel="shortcut icon" href="./images/favicon.png">
		<link rel="stylesheet" type="text/css" href="./css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="./css/style.css" />
		<link rel="stylesheet" type="text/css" href="./css/mouse-scroll.css" />
		<link rel="stylesheet" type="text/css" href="./css/hamburgers.css" />
		
		<script src="./js/modernizr.custom.js"></script>
		
		<!--FANCYBOX FOR VIDEO BUTTON-->
		<link rel="stylesheet" href="./fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen">
		<link rel="stylesheet" href="./fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen">
		<link rel="stylesheet" href="./fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen">
		
	</head>
	<body class="loading">
		<div id="animationStation" class="container navbar-top">
			
			<div id="header">
				<div class="navbar">
					<div class="contained">
						<nav>
							<ul>
								<li><a href="#the-bolt-junior-intro" class="boltNav">Bolt Jr.</a></li>
								<li><a href="#features" class="featuresNav">Features</a></li>
								<li><a href="#flair" class="flairNav">flair</a></li>
								<li><a href="#specs" class="specsNav">Specs</a></li>
								<li><a href="#contact" class="contactNav">Contact Us</a></li>
								<li><a href="https://www.mrmoco.com/">Main Site</a></li>
							</ul>
						</nav>
						<div class="logo"><a href="./"><img src="./images/Bolt-Jr-Logo.svg" alt="The Bolt Junior" /></a></div>
						<button class="ozzle hamburger hamburger--spin" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
			</div>
			
			<section id="intro">
				<div id="fluid_video_container" class="top-vid">
					<div class="fancybox-media" href="https://www.youtube.com/watch?v=MiS4AEQj_yQ">
						<div class="play-btn">
							<svg width="121px" height="121px" viewBox="0 0 121 121" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g id="Desktop-HD" transform="translate(-660.000000, -290.000000)">
													<g id="Play-Btn" transform="translate(663.000000, 293.000000)">
															<circle id="Oval" stroke="#EA6429" stroke-width="4" cx="57.5" cy="57.5" r="57.5"></circle>
															<polygon id="Triangle" points="80 57.5 46 79 46 36"></polygon>
													</g>
											</g>
									</g>
							</svg>
						</div>
					</div>
					
					<div class="overlay"></div>
					
					<video id="fluid_video" muted="muted" preload="metadata" autoplay="autoplay" loop="loop">
						<source type="video/mp4" />
					</video>
					
					<div class="videoLoad"></div>
					
					<a href="#the-bolt-junior-intro">
						<div class="scroll-down svg" id="home-scroll-down">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_2" x="0px" y="0px" viewBox="0 0 25.166666 37.8704414" enable-background="new 0 0 25.166666 37.8704414" xml:space="preserve">
									<path class="stroke" fill="none" stroke="#EA6429" stroke-width="1.5" stroke-miterlimit="10" d="M12.5833445 36.6204414h-0.0000229C6.3499947 36.6204414 1.25 31.5204487 1.25 25.2871208V12.5833216C1.25 6.3499947 6.3499951 1.25 12.5833216 1.25h0.0000229c6.2333269 0 11.3333216 5.0999947 11.3333216 11.3333216v12.7037992C23.916666 31.5204487 18.8166714 36.6204414 12.5833445 36.6204414z"></path>
									<path class="scroller" fill="#c7c4b8" d="M13.0833359 19.2157116h-0.9192753c-1.0999985 0-1.9999971-0.8999996-1.9999971-1.9999981v-5.428606c0-1.0999994 0.8999987-1.9999981 1.9999971-1.9999981h0.9192753c1.0999985 0 1.9999981 0.8999987 1.9999981 1.9999981v5.428606C15.083334 18.315712 14.1833344 19.2157116 13.0833359 19.2157116z"></path>
							</svg><i class="icon icon-arrow-down"></i>
						</div>
					</a>
				</div>
			</section>
			
			<section id="the-bolt-junior-intro" class="waypoint" data-animate-down="navbar-scrolled hello-bolt-intro" data-animate-up="navbar-top">
				<div class="fullWidth center paddedTopBottom">
					<div class="contained">
						<div class="">
							<h1 class="orange">Meet the high-speed Bolt&nbsp;Jr.</h1>
							<p>Bolt Jr. is a compact, lighter weight high-speed camera robot that’s an agile, smaller option than the full scale MRMC Bolt. The Bolt Jr. is an ideal solution where space, weight, mobility and budget&nbsp;are&nbsp;key.</p>
						</div>
					</div>
				</div>
			</section>
			<section id="the-bolt-junior" class="waypoint" data-animate-down="navbar-scrolled hello-bolt" data-animate-up="navbar-top bye-bolt hello-bolt-intro">
				<div class="halfSection greyBack">
					<div class="title onLeft">
						<div class="lineDeco">
							<hr class="top"/>
							<hr class="bottom"/>
						</div>
						<div class="showUp textTop">
							<h1 class="light">Small but mighty</h1>
							<p>Small in stature but in no means short of capability, Bolt Jr. is a compact camera robot capable of impressive and sophisticated high-speed, automated image capture in a wide range of applications – including commercials, tabletop, broadcast, film and photographic&nbsp;applications.<br><br>Bolt Jr. is an ideal choice for image capture in tight studios and on-set&nbsp;locations.</p>
						</div>
					</div>
					<div class="bottom30 onLeft center">
						<div class="toNextSection">
							<a href="#features">Key Features</a>
							<a href="#features"><img src="./images/down-btn.svg" width="40" height="40" alt="Key Features" /></a>
						</div>
					</div>
				</div>
				<div class="halfSection greyBackDarkest">
					<div class="img1">
					</div>
				</div>
			</section>
			
			<section id="features" class="waypoint" data-animate-down="navbar-scrolled hello-features bye-bolt" data-animate-up="navbar-scrolled hello-bolt bye-features">
				<div class="halfSection greyBackDarkest">
					<div class="img2">
					</div>
				</div>
				<div class="halfSection greyBack">
					<div class="title onRight">
						<div class="lineDeco">
							<hr class="top"/>
							<hr class="bottom"/>
						</div>
						<div class="showUp">
							<h1 class="light">Key features</h1>
						</div>
					</div>
					<div class="showUp centerVert paddedSides">
						<ul>
							<li>Small, light high-speed robotic&nbsp;arm</li>
							<li>Only 110kg weight (under half weight of the&nbsp;Bolt)</li>
							<li>Camera payload up to&nbsp;12kg</li>
							<li>Can be used as both camera rig or model&nbsp;mover</li>
							<li>Track speed in excess of&nbsp;3m/sec</li>
							<li>Arm reach 1.2m</li>
						</ul>
					</div>
					<div class="bottom30 onRight center">
						<div class="toNextSection">
							<a href="#flair">Flair Software</a>
							<a href="#flair"><img src="./images/down-btn.svg" width="40" height="40" alt="Get in touch" /></a>
						</div>
					</div>
				</div>
			</section>
			
			<section id="flair" class="waypoint centerVert" data-animate-down="navbar-scrolled hello-flair bye-features" data-animate-up="navbar-scrolled hello-features bye-flair">
				<div class="halfSection">
					<div class="title onLeft">
						<div class="lineDeco">
							<hr class="top"/>
							<hr class="bottom"/>
						</div>
						<div class="showUp textTop">
							<h1 class="light">The Flair Software</h1>
						</div>
					</div>
					<div class="flairContainer">
						<div class="flairImage showUp">
							<img src="./images/flair-desktop-screen.png">
						</div>
						<h3>Powerful, intuitive software to get you creating amazing shots in no&nbsp;time!</h3>
					</div>
					<div class="bottom30 onLeft center">
						<div class="toNextSection">
							<a href="#specs">Specifications</a>
							<a href="#specs"><img src="./images/down-btn.svg" width="40" height="40" alt="Specifications" /></a>
						</div>
					</div>
				</div>
				<div class="halfSection">
					<div class="cardGrid">
						
						<div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Turntable.svg">
									<h3>Turntables</h3>
								</div>
							</div> 
							<div class="back">
								<p>Synchronised motion with external triggers and model movers  for extreme precision timing</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Portable.svg">
									<h3>Portable</h3>
								</div>
							</div> 
							<div class="back">
								<p>Portable, lightweight robotic unit, designed to be set-up and ready for shooting  in under an hour!</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Timecode.svg">
									<h3>Timecode</h3>
								</div>
							</div> 
							<div class="back">
								<p>Trigger from timecode</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/High-Speed.svg">
									<h3>High Speed</h3>
								</div>
							</div> 
							<div class="back">
								<p>Ultra-high speed and precision in a compact rig</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Timelapse.svg">
									<h3>Timelapse</h3>
								</div>
							</div> 
							<div class="back">
								<p>Create incredible timelapse with dynamic movement and feel</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Repeatable.svg">
									<h3>Repeatable</h3>
								</div>
							</div> 
							<div class="back">
								<p>Every camera move under complete control - precisely repeatable!</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Target-Tracking.svg">
									<h3>Target Tracking</h3>
								</div>
							</div> 
							<div class="back">
								<p>Define location of your object in space, to simplify any complex move</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Lens-Control.svg">
									<h3>Lens Control</h3>
								</div>
							</div> 
							<div class="back">
								<p>High speed, precise control of  zoom, iris &amp; lens</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Maya.svg">
									<h3>Maya</h3>
								</div>
							</div> 
							<div class="back">
								<p>Take moves planned in Maya and feed them into the&nbsp;Bolt</p>
							</div> 
						</div><!--
						
						--><div id="card" class="card">
							<div class="front">
								<div>
									<img src="./images/icons/Lighting.svg">
									<h3>Lighting</h3>
								</div>
							</div> 
							<div class="back">
								<p>Control DMX Lighting</p>
							</div> 
						</div>
						
					</div>
				</div>
			</section>
			
			<section id="specs" class="waypoint" data-animate-down="navbar-scrolled hello-specs bye-flair" data-animate-up="navbar-scrolled hello-flair bye-specs">
				<div class="greyBackDark fullWidth">
					<div class="title onLeft">
						<div class="lineDeco">
							<hr class="top"/>
							<hr class="bottom"/>
						</div>
						<div class="showUp">
							<h1 class="light">Specifications</h1>
						</div>
					</div>
					<div class="showUp centerVert paddedSides table">
						<div class="specsTable">
							<div>
								<div class="tableDiv">
									<table>
										<tr>
											<td class="first">
												<h3>Axis</h3>
											</td>
											<td>
												<h3>Travel</h3>
											</td>
											<td>
												<h3>Max Speed</h3>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Rotate</p>
											</td>
											<td>
												<p>+/- 160º</p>
											</td>
											<td>
												<p>400º/s</p>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Lift</p>
											</td>
											<td>
												<p>+/- 147º</p>
											</td>
											<td>
												<p>390º/s</p>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Arm</p>
											</td>
											<td>
												<p>+/- 145º</p>
											</td>
											<td>
												<p>420º/s</p>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Pan</p>
											</td>
											<td>
												<p>+/- 270º</p>
											</td>
											<td>
												<p>540º/s</p>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Tilt</p>
											</td>
											<td>
												<p>+/- 140º /-115º</p>
											</td>
											<td>
												<p>475º/s</p>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Roll</p>
											</td>
											<td>
												<p>+/- 270º</p>
											</td>
											<td>
												<p>760º/s</p>
											</td>
										</tr>
										<tr>
											<td class="first">
												<p>Track</p>
											</td>
											<td>
												<p>Unlimited</p>
											</td>
											<td>
												<p>3.5m/s</p>
											</td>
										</tr>
									</table>
								</div>
								<div class="button">
									<a href="./downloads/bolt-junior-spec-sheet.pdf" target="_blank">Download Specs</a>
								</div>
							</div>
						</div>
					</div>
					<div class="bottom30 inMiddle center">
						<div class="toNextSection">
							<a href="#contact">Get in touch</a>
							<a href="#contact"><img src="./images/down-btn.svg" width="40" height="40" alt="Get in touch" /></a>
						</div>
					</div>
				</div>
			</section>
			
			<section id="contact" class="waypoint" data-animate-down="navbar-scrolled hello-contact bye-specs" data-animate-up="navbar-scrolled hello-specs bye-contact">
				<div class="halfSection greyBackDarkest">
					<div class="title onLeft">
						<div class="lineDeco">
							<hr class="top"/>
							<hr class="bottom"/>
						</div>
						<div class="showUp textTop">
							<h1 class="light">Contact us</h1>
						</div>
					</div>
					<div class="showUp centerVert paddedSides form">
						<?php include('./contact/contact-form.php') ?>
					</div>
				</div>
				<div class="halfSection greyBackDark">
					<!--GOOGLE MAPS AND CUSTOMIZATION-->
					<?php include('./googlemaps.php')?>
				</div>
			</section>
			
			<section id="boltBackground">
				<div class="comparison-bolt"></div>					
			</section>
			
			<section class="notFullHeight greyBack">
				<div class="boltCompare">
					<div class="button">
						<a href="https://www.mrmoco.com/thebolt/" target="_blank">Compare the Bolt</a>
					</div>
				</div>
			</section>
			
			<footer class="greyBackDarkest">
				<div class="greyBackDarker twoThirds footer">
					
					<a href="https://mrmoco.com" target="_blank">
						<img src="./images/MRMC.svg" alt="Mark Roberts Motion Control" class="mrmc" />
					</a>
					
					<p><span>t:</span> <a href="tel:+441342838000" class="link">+44 (0)1342 838000</a><br>
						<span>e:</span> <a href="mailto:info@mrmoco.com" class="link">info@mrmoco.com</a></p>

					<p>Head Office (UK)<br>
						Mark Roberts Motion Control<br>
						Unit 3, South East Studios,<br>
						Eastbourne Road, Blindley Heath,<br>
						Surrey, RH7 6JP, UK.
					</p>
					
					<p><span class="dark">VAT: GB 200-0201-71</span></p>
					<span class="copyright">&copy; <?php echo date("Y"); ?> Mark Roberts Motion Control</span>
				</div><!--
				
		 --><div class="oneThird social">
					<h3>Let's get social</h3>
					<ul>
						<li class="social-icon">
							<a href="https://twitter.com/mrmoco1" target="_blank">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve" width="30px" height="30px">
									<g>
										<g>
											<path d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411    c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513    c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101    c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104    c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194    c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485    c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z" class="fillSVG"/>
										</g>
									</g>
								</svg>
							</a>
						</li>
						<li class="social-icon">
							<a href="https://www.facebook.com/mrmoco" target="_blank">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 486.392 486.392" style="enable-background:new 0 0 486.392 486.392;" xml:space="preserve" width="30px" height="30px">
									<g>
										<g>
											<path d="M273.443,159.354l0.122-41.951c0-21.857,1.52-33.561,32.831-33.561h57.941V0h-83.021     c-80.559,0-99.102,41.617-99.102,109.985l0.091,49.369l-61.133,0.03v83.811h61.133v243.196h91.168l0.061-243.196l82.778-0.03     l8.907-83.811H273.443z" class="fillSVG"/>
										</g>
									</g>
								</svg>
							</a>
						</li>
						<li class="social-icon">
							<a href="https://www.youtube.com/user/mrmoco1" target="_blank">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 478.165 478.165" style="enable-background:new 0 0 478.165 478.165;" xml:space="preserve" width="30px" height="30px">
									<g>
										<g>
											<path d="M376.893,349.758h-26.16l0.12-15.301c0-6.654,5.479-12.372,12.253-12.372h1.634   c7.013,0,12.612,5.718,12.612,12.372L376.893,349.758z M278.491,317.063c-6.654,0-12.034,4.562-12.034,10.041v74.016   c0,5.379,5.379,10.041,12.034,10.041s12.133-4.662,12.133-10.041v-74.016C290.624,321.625,285.146,317.063,278.491,317.063z    M438.298,275.622v140.899c0,33.85-29.188,61.643-65.15,61.643H105.117c-35.962,0-65.25-27.793-65.25-61.643V275.622   c0-33.85,29.308-61.404,65.25-61.404h268.031C409.11,214.218,438.298,241.772,438.298,275.622z M122.988,431.942v-148.49h33.272   v-22.055l-88.6-0.12v21.717h27.674v148.968h27.654V431.942z M222.566,305.627h-27.674v79.156c0,11.675,0.697,17.274,0,19.386   c-2.331,6.077-12.373,12.612-16.337,0.697c-0.697-2.331-0.12-8.408-0.12-19.386v-79.853h-27.674l0.12,78.678   c0,11.914-0.239,21.019,0.12,24.984c0.578,7.232,0.359,15.64,7.013,20.541c12.492,8.866,36.42-1.395,42.377-14.245v16.337h22.175   V305.627L222.566,305.627z M311.166,396.438v-66.186c0-24.984-18.788-40.166-44.37-19.844l0.12-49.032h-27.793v169.629   l22.653-0.458l2.211-10.5C292.956,446.646,311.166,428.436,311.166,396.438z M397.913,387.572l-20.78,0.239   c0,0.697,0,1.634,0,2.809v11.675c0,6.077-5.14,11.217-11.436,11.217h-3.965c-6.296,0-11.317-5.14-11.317-11.217v-1.395v-12.851   v-16.576h47.518v-17.851c0-12.851-0.458-26.16-1.395-33.511c-3.507-23.689-36.659-27.315-53.475-15.301   c-5.379,3.746-9.344,8.746-11.675,15.64c-2.331,6.774-3.507,16.118-3.507,28.012v39.349   C327.981,453.181,407.237,443.836,397.913,387.572z M291.322,173.932c1.514,3.507,3.746,6.296,6.654,8.408   c3.028,2.092,6.894,3.267,11.436,3.267c3.965,0,7.471-1.175,10.619-3.507c3.028-2.212,5.599-5.479,7.83-9.922l-0.697,10.978h30.941   V51.721h-24.287v102.267c0,5.599-4.562,10.041-10.281,10.041c-5.479,0-10.161-4.562-10.161-10.041V51.721h-25.223v88.6   c0,11.217,0.239,18.788,0.458,22.653C288.991,166.7,290.047,170.446,291.322,173.932z M197.821,99.697   c0-12.612,1.056-22.414,3.148-29.527c1.992-7.013,5.838-12.731,11.317-17.035c5.479-4.303,12.373-6.415,20.9-6.415   c7.113,0,13.309,1.514,18.449,4.084c5.021,2.909,8.986,6.415,11.675,10.858c3.028,4.443,4.901,8.986,5.838,13.668   c1.056,4.662,1.514,11.914,1.514,21.358v33.392c0,12.133-0.359,21.139-1.514,26.857c-0.817,5.718-2.809,10.978-5.957,15.879   c-3.028,4.901-7.013,8.527-11.914,10.858c-4.782,2.57-10.38,3.626-16.815,3.626c-7.013,0-12.851-1.056-17.752-3.028   c-5.021-2.092-8.746-5.021-11.436-9.105c-2.69-3.965-4.662-8.986-5.718-14.584c-1.175-5.838-1.753-14.484-1.753-26.04V99.697   H197.821z M221.988,151.877c0,7.471,5.599,13.428,12.253,13.428c6.774,0,12.253-5.957,12.253-13.428V81.945   c0-7.352-5.599-13.428-12.253-13.428c-6.774,0-12.253,6.077-12.253,13.428V151.877z M136.416,187.241h29.188l0.12-100.753   l34.328-86.269h-31.758l-18.33,63.974L131.396,0H99.877l36.54,86.508C136.416,86.508,136.416,187.241,136.416,187.241z" class="fillSVG"/>
										</g>
									</g>
								</svg>
							</a>
						</li>
						<li class="social-icon">
							<a href="https://vimeo.com/mrmoco" target="_blank">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 97.097 97.096" style="enable-background:new 0 0 97.097 97.096;" xml:space="preserve" width="30px" height="30px">
									<g>
										<g>
											<path d="M97.05,25.977c-0.431,9.453-7.038,22.401-19.806,38.834C64.046,81.975,52.876,90.56,43.734,90.56   c-5.655,0-10.444-5.226-14.357-15.684c-2.613-9.585-5.227-19.161-7.838-28.746c-2.902-10.452-6.017-15.683-9.352-15.683   c-0.724,0-3.27,1.531-7.622,4.577L0,29.136c4.788-4.208,9.517-8.422,14.167-12.643c6.394-5.52,11.19-8.429,14.391-8.722   c7.557-0.725,12.208,4.446,13.954,15.502c1.886,11.938,3.191,19.361,3.922,22.264c2.176,9.902,4.576,14.849,7.19,14.849   c2.034,0,5.091-3.216,9.159-9.634c4.065-6.427,6.244-11.315,6.537-14.667c0.58-5.545-1.6-8.324-6.537-8.324   c-2.328,0-4.727,0.531-7.188,1.592c4.77-15.632,13.887-23.229,27.348-22.8C92.923,6.847,97.623,13.322,97.05,25.977z" class="fillSVG"/>
										</g>
									</g>
								</svg>
							</a>
						</li>
					</ul>
				</div>
				<a href="http://digitaldesign.works" target="_blank" class="ddwCredits"><span style="font-weight: 300;">site by</span> digitaldesign.works</a>
			</footer>
			
		</div><!-- /container -->
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="./js/waypoints.min.js"></script>
		
		<script>
			var $head = $( '#animationStation' );
			$( '.waypoint' ).each( function(i) {
				var $el = $( this ),
					animClassDown = $el.data( 'animateDown' ),
					animClassUp = $el.data( 'animateUp' );

				$el.waypoint( function( direction ) {
					if( direction === 'down' && animClassDown ) {
						$head.attr('class', 'container ' + animClassDown);
					}
					else if( direction === 'up' && animClassUp ){
						$head.attr('class', 'container ' + animClassUp);
					}
				}, { offset: '50%' } );
			} );
		</script>
		
		
		<!--FANCYBOX JS-->
		
		<script type="text/javascript" src="./fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
		<script type="text/javascript" src="./fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript" src="./fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<script type="text/javascript" src="./fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		<script type="text/javascript" src="./fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
		
		<!--PRELOAD ALL IMAGES/VIDEO-->
		<script src="./js/imagesloaded.pkgd.min.js"></script>
			
		<!--FLIP JS-->
		<script src="./js/jquery.flip.min.js"></script>
		
		<!--MAIN CUSTOM JS FOR BOLT JR-->
		<script src="./js/main.js"></script>
		
	</body>
</html>
// Digital Design Works

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1200, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });

//hamburger menu
$( ".hamburger, .navbar nav a" ).click(function() {
  $( ".hamburger, .navbar nav" ).toggleClass('is-active');
});

//HTML Video load/don't load for mobile
$(document).ready(function() { 

	if ($(window).width() < 990) {

		// If mobile, then we do all this
		$('.fancybox-media').fancybox({
			openEffect  : 'fade',
			closeEffect : 'fade',
			helpers : {
				media : {}
			}
		});
		
		$('.container, .img1, .img2, section#boltBackground, .comparison-bolt').imagesLoaded( { background: true }, function() {
			$('body').removeClass('loading');	
		});

	}
	else {

		// If not mobile then do this
		var videoFile = './video/junior-bolt-small.mp4';
		$('#fluid_video_container video source').attr('src', videoFile);
		$("#fluid_video_container video")[0].load();
		
		$('.container, .img1, .img2, .videoLoad, section#boltBackground, .comparison-bolt').imagesLoaded( { background: true }, function() {
			$('body').removeClass('loading');	
		});

	}
});

//flip card

$(".card").flip({trigger: 'hover'});

